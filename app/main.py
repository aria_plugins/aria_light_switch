from flask import Flask, request
from flask.views import MethodView

app = Flask(__name__)
lights = {}

@app.errorhandler(500)
def internal_error(error):
    return "Cannot retrieve what you are looking for right now"

class Request(MethodView):
    def post(self):
        global lights
        outcome = request.json['outcome']
        scheduled = False
        delta=0
        specific_location = False
        location=""
        value="on"
        print(outcome)
        if 'entities' in outcome:
            if 'on_off' in outcome['entities']:
                if len(outcome['entities']['on_off']) > 0:
                    if 'value' in outcome['entities']['on_off'][0]:
                        value = outcome['entities']['on_off'][0]['value']
            if 'location' in outcome['entities']:
                if len(outcome['entities']['location']) > 0:
                    if 'value' in outcome['entities']['location'][0]:
                        location = outcome['entities']['location'][0]['value'].lower()
                        specific_location = True
            if 'color' in outcome['entities']:
                if len(outcome['entities']['color']) > 0:
                    if 'value' in outcome['entities']['color'][0]:
                        location = outcome['entities']['color'][0]['value'].lower()
                        specific_location = True

        response = ""
        if specific_location == False:
            for key, val in lights.items():
                if value == "on":
                    lights[key] = True
                elif value == "off":
                    lights[key] = False
                elif value == "toggle":
                    lights[key] = not lights[key]
            if value == "on":
                response = "All the lights are on __lights__:all:on"
            elif value == "off":
                response = "All the lights are off __lights__:all:off"
            elif value == "toggle":
                if (lights['all']):
                    response = "All the lights are on __lights__:all:on"
                else:
                    response = "All the lights are off __lights__:all:off"
        else:
            if value == "on":
                if location in lights and lights[location] == True:
                    response = "This light is already on __lights__:" + location + ":on"
                else:
                    lights[location] = True
                    response = "Light switched on __lights__:" + location + ":on"
            elif value == "off":
                if location in lights and lights[location] == False:
                    response = "This light is already off __lights__:" + location + ":off"
                else:
                    lights[location] = False
                    response = "Light switched off __lights__:" + location + ":off"
            elif value == "toggle":
                if not location in lights:
                    lights[location]
                lights[location] = not lights[location]
                if (lights[location] == False):
                    response = "Light switched on __lights__:" + location + ":on"
                else:
                    response = "Light switched off __lights__:" + location + ":off"
        return response


if __name__ == '__main__':
    app.add_url_rule('/', view_func=Request.as_view('request'))
    app.run(host='0.0.0.0', port=80)
